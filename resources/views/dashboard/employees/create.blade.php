@extends('dashboard.layouts.app')

@section('title', 'Employees | Create')

@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-5 text-gray-800">Employees</h1>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Create Company</h6>
            </div>
            <div class="card-body">
                <form method="post" action="{{ url('admin/employees') }}" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group">
                        <label for="exampleInputName1">Image</label>
                        <input type="file"
                               name="image"
                               value="{{ old('image') }}"
                               class="form-control @error('image') is-invalid @enderror"
                               id="exampleInputName1"
                               aria-describedby="imageHelp" />

                        @error('image')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="exampleInputName1">First Name</label>
                        <input type="text"
                               name="first_name"
                               value="{{ old('first_name') }}"
                               class="form-control @error('first_name') is-invalid @enderror"
                               id="exampleInputName1"
                               aria-describedby="nameHelp" />

                        @error('first_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="exampleInputName1">Last Name</label>
                        <input type="text"
                               name="last_name"
                               value="{{ old('last_name') }}"
                               class="form-control @error('last_name') is-invalid @enderror"
                               id="exampleInputName1"
                               aria-describedby="nameHelp" />

                        @error('last_name')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="exampleInputName1">Company</label>

                        <select class="custom-select mb-3 @error('company_id') is-invalid @enderror" name="company_id">
                            <option selected value="">select company</option>
                            @foreach($companies as $company)
                                <option value="{{ $company->id }}" {{ $company->id == old('company_id') ? 'selected' : '' }}>{{ $company->name }}</option>
                            @endforeach
                        </select>

                        @error('company_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email"
                               name="email"
                               value="{{ old('email') }}"
                               class="form-control @error('email') is-invalid @enderror"
                               id="exampleInputEmail1"
                               aria-describedby="emailHelp" />

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Phone</label>
                        <input type="text"
                               name="phone"
                               value="{{ old('phone') }}"
                               class="form-control @error('phone') is-invalid @enderror"
                               id="exampleInputPhone1"
                               aria-describedby="phoneHelp" />

                        @error('phone')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Website Link</label>
                        <input type="text"
                               name="website"
                               value="{{ old('website') }}"
                               class="form-control @error('website') is-invalid @enderror"
                               id="exampleInputPassword1" />

                        @error('website')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-check"></i>
                        Save
                    </button>
                </form>
            </div>
        </div>
    </div>
@stop
