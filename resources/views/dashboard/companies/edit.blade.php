@extends('dashboard.layouts.app')

@section('title', 'Companies | Edit')

@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-5 text-gray-800">Companies</h1>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Edit Company</h6>
            </div>
            <div class="card-body">
                <form method="post" action="{{ url("admin/companies/$company->id") }}">
                    {{ method_field('PUT') }}
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text"
                               name="name"
                               value="{{ $company->name }}"
                               class="form-control @error('name') is-invalid @enderror"
                               id="exampleInputEmail1"
                               aria-describedby="emailHelp" />

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email"
                               name="email"
                               value="{{ $company->email }}"
                               class="form-control @error('email') is-invalid @enderror"
                               id="exampleInputEmail1"
                               aria-describedby="emailHelp" />

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Website Link</label>
                        <input type="text"
                               name="website"
                               value="{{ $company->website }}"
                               class="form-control @error('website') is-invalid @enderror"
                               id="exampleInputPassword1" />

                        @error('website')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-check"></i>
                        Save
                    </button>
                </form>
            </div>
        </div>
    </div>
@stop
