@extends('dashboard.layouts.app')

@section('title', 'Companies')

@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800">Companies</h1>

        <!-- BEGIN :: alert session -->
        @include('dashboard.components.alert-session')
        <!-- END :: alert session -->

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary float-left">Companies Table</h6>

                <button type="button" class="btn btn-outline-primary float-right" onclick="location.href='{{ url('admin/companies/create') }}'">
                    <i class="fa fa-plus"></i>
                    Create
                </button>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>website</th>
                            <th>Tools</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>website</th>
                            <th>Tools</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @forelse($companies as $company)
                            <tr>
                                <td>{{ $company->name }}</td>
                                <td>{{ $company->email }}</td>
                                <td>{{ $company->website }}</td>
                                <td>
                                    <a class="btn btn-primary" href="{{ url("admin/companies/$company->id/edit") }}" role="button">
                                        <i class="fa fa-pencil"></i>
                                    </a>

                                    <button class="btn btn-danger"
                                            data-toggle="modal"
                                            data-target="#deleteModal"
                                            data-url="{{ url("admin/companies/$company->id") }}"
                                            id="delete-company"
                                            role="button">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td></td>
                                <td colspan="2">Not Found Data</td>
                                <td></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
                {{ $companies->links() }}
            </div>
        </div>
    </div>

    <!-- Begin : include delete modal -->
    @include('dashboard.components.delete-modal', [
        'form_id' => 'delete-company-form',
        'delete_title' => 'Company',
        'btn_delete_id' => '#delete-company'
    ])
    <!-- End : include delete modal -->
@stop
