<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('admin') }}">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Dashboard <sup>2</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item {{ \Request::is('admin') ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('admin') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Nav Item - companies -->
    <li class="nav-item {{ \Request::is('admin/companies*') ? 'active' : '' }}">
        <a class="nav-link collapsed" href="{{ url('admin/companies') }}">
            <i class="fa fa-building"></i>
            <span>Companies</span>
        </a>
    </li>

    <!-- Nav Item - employees -->
    <li class="nav-item {{ \Request::is('admin/employees*') ? 'active' : '' }}">
        <a class="nav-link collapsed" href="{{ url('admin/employees') }}">
            <i class="fa fa-users"></i>
            <span>Employees</span>
        </a>
    </li>
</ul>
