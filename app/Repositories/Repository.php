<?php


namespace App\Repositories;


use Illuminate\Database\Eloquent\Model;

abstract class Repository
{
    protected $model;

    public function __construct($model)
    {
        $this->model($model);
    }

    /**
     * Set repository model.
     *
     * @param Model $model
     */
    public function model(Model $model)
    {
        $this->model = $model;
    }
}
