<?php


namespace App\Repositories\Companies;


use App\Enums\GeneralEnums;
use App\Models\Company;
use App\Repositories\EloquentRepositoryInterface;
use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Collection;


class CompaniesRepository extends Repository implements EloquentRepositoryInterface
{
    public function __construct(Company $model)
    {
        parent::__construct($model);
    }

    /**
     * List companies.
     *
     * @param int $perPage
     * @return Collection
     */
    public function get($perPage = GeneralEnums::perPage)
    {
        return $this->model->paginate($perPage);
    }

    /**
     * Get specification model by key.
     *
     * @param int $key
     * @return Model
     */
    public function find(int $key): Model
    {
        return $this->model->where('id', $key)->first();
    }

    /**
     * Create new model.
     *
     * @param array $data
     * @return Model
     */
    public function create(array $data): Model
    {
        return $this->model->create($data)->image()->create(['image' => $data['image']->store('companies', 'public')]);
    }

    /**
     * Update model.
     *
     * @param Model $model
     * @param array $data
     * @return bool
     */
    public function update(Model $model, array $data): bool
    {
        return $model->update($data);
    }

    /**
     * Delete model.
     *
     * @param Model $model
     * @return bool
     * @throws \Exception
     */
    public function delete(Model $model): bool
    {
        return $model->delete();
    }

    /**
     * List model.
     *
     * @return mixed
     */
    public function list()
    {
        return $this->model->get();
    }
}
