<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'email',
        'website',
    ];

    public static function boot() {
        parent::boot();

        # before delete() method call this
        static::deleting(function($company) {
            $company->image()->delete();
            # do the rest of the cleanup...
        });
    }

    /**
     * Get the post's image.
     */
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
}
