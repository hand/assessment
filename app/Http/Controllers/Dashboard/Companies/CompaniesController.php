<?php

namespace App\Http\Controllers\Dashboard\Companies;

use App\Http\Controllers\Controller;
use App\Http\Requests\Companies\CreateCompaniesRequest;
use App\Http\Requests\Companies\UpdateCompaniesRequest;
use App\Models\Company;
use App\Repositories\Companies\CompaniesRepository;

class CompaniesController extends Controller
{
    protected $model;

    public function __construct(CompaniesRepository $companiesRepository)
    {
        $this->model = $companiesRepository;
    }

    /**
     * Display a listing of the companies.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        # List companies.
        $companies = $this->model->get();

        return view('dashboard.companies.index', compact('companies'));
    }

    /**
     * Show the form for creating a new company.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('dashboard.companies.create');
    }

    /**
     * Store a newly created company in storage.
     *
     * @param CreateCompaniesRequest $request
     */
    public function store(CreateCompaniesRequest $request)
    {
        if($this->model->create($request->except('_token')))
            return redirect('admin/companies')->with('success', 'Company Stored Successfully');

        return redirect('admin/companies')->with('error', "We could\'not handle your request, please try again later");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified company.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(int $id)
    {
        # get specification company by id.
        $company = $this->model->find($id);

        return view('dashboard.companies.edit', compact('company'));
    }

    /**
     * Update the specified company in storage.
     *
     * @param Company $company
     * @param UpdateCompaniesRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Company $company, UpdateCompaniesRequest $request)
    {
        # update company.
        if($this->model->update($company, $request->except('_method', '_token')))
            return redirect('admin/companies')->with('success', 'Company updated Successfully');

        return redirect('admin/companies')->with('error', "We could\'not handle your request, please try again later");
    }

    /**
     * Remove the specified company from storage.
     *
     * @param Company $company
     * @throws \Exception
     */
    public function destroy(Company $company)
    {
        # delete company.
        if($this->model->delete($company))
            return redirect('admin/companies')->with('success', 'Company Deleted Successfully');

        return redirect('admin/companies')->with('error', "We could\'not handle your request, please try again later");
    }
}
