<?php

namespace App\Http\Controllers\Dashboard\Employees;

use App\Http\Controllers\Controller;
use App\Http\Requests\Employees\CreateEmployeesRequest;
use App\Http\Requests\Employees\UpdateEmployeesRequest;
use App\Models\Employee;
use App\Repositories\Companies\CompaniesRepository;
use App\Repositories\Employees\EmployeesRepository;

class EmployeesController extends Controller
{
    protected $employeeModel;
    protected $companiesModel;

    public function __construct(EmployeesRepository $employeesRepository, CompaniesRepository $companiesRepository)
    {
        $this->employeeModel = $employeesRepository;
        $this->companiesModel = $companiesRepository;
    }

    /**
     * Display a listing of the employees.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        # List employees.
        $employees = $this->employeeModel->get();

        return view('dashboard.employees.index', compact('employees'));
    }

    /**
     * Show the form for creating a new employee.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        # list companies.
        $companies = $this->companiesModel->list();

        return view('dashboard.employees.create', compact('companies'));
    }

    /**
     * Store a newly created employee in storage.
     *
     * @param CreateEmployeesRequest $request
     */
    public function store(CreateEmployeesRequest $request)
    {
        if($this->employeeModel->create($request->except('_token')))
            return redirect('admin/employees')->with('success', 'Employee Stored Successfully');

        return redirect('admin/employees')->with('error', "We could\'not handle your request, please try again later");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified employee.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(int $id)
    {
        # get specification employee by id.
        $employee = $this->employeeModel->find($id);

        # list companies.
        $companies = $this->companiesModel->list();

        return view('dashboard.employees.edit', compact('employee', 'companies'));
    }

    /**
     * Update the specified employee in storage.
     *
     * @param Employee $employee
     * @param UpdateEmployeesRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Employee $employee, UpdateEmployeesRequest $request)
    {
        # update employee.
        if($this->employeeModel->update($employee, $request->except('_method', '_token')))
            return redirect('admin/employees')->with('success', 'Employee updated Successfully');

        return redirect('admin/employees')->with('error', "We could\'not handle your request, please try again later");
    }

    /**
     * Remove the specified employee from storage.
     *
     * @param Employee $employee
     * @throws \Exception
     */
    public function destroy(Employee $employee)
    {
        # delete employee.
        if($this->employeeModel->delete($employee))
            return redirect('admin/employees')->with('success', 'Employee Deleted Successfully');

        return redirect('admin/employees')->with('error', "We could\'not handle your request, please try again later");
    }
}
